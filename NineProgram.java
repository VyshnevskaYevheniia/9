package week1;

public class NineProgram {
    public static void main(String args[]) {
        int arr[] = {1, 5, 33, 12, 88, 9, 192, 123, 567, 88, 44, 32};
        int sum = 0;
        for (int x : arr) {
            System.out.println("Value is: " + x);
            sum += x;
        }
        System.out.println("Sum: " + sum);

        int i = 0;
        while (i < arr.length) {
            System.out.println(arr[i]);
            i++;
        }
        //for (int i = 0; i < 10; i++) sum += nums[i];

        int r = 0;
        do {
            System.out.println(arr[r]);
            r++;
        } while (r < arr.length);

        for (int h = 0; h < arr.length; h++) {
            System.out.println(arr[h]);
        }

    }
}